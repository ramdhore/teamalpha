// Two char difference
import java.io.*;
class patt6
{
    public static void main(String[] args)throws IOException
    {
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
       
        System.out.println("Enter  the char1:");
        char ch1=(char)br.read();
        //Use skip method otherwise it will take \n as input in char2
        br.skip(2);
        System.out.println("Enter  the char2:");
        char ch2=(char)br.read();
        
        if(ch1==ch2){
            System.out.println(ch1);
            System.out.println(ch2);
        }else{
            if(ch1>ch2){
                System.out.println("Difference is:"+(ch1-ch2));
            }else{
                System.out.println("Difference is:"+(ch2-ch1));

            }
        }
        
        
    }
}
