import java.io.*;
class patt3
{
    public static void main(String[] args)throws IOException
    {
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter  the rows:");
        int r=Integer.parseInt(br.readLine());
        int num=r;
        for(int i=1;i<=r;i++){
           int num1=num*i;
            for(int j=1;j<=r-i+1;j++){
                System.out.print(num1+" ");
                num1-=i;
            }
            num--;
            System.out.println();
        }
    }
}
