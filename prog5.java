//fibo  series
import java.io.*;
class patt5
{
    public static void main(String[] args)throws IOException
    {
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter  the rows:");
        int r=Integer.parseInt(br.readLine());
        int num1=0;
        int num2=1;
        int num3;
        for(int i=1;i<=r;i++){
           
            for(int j=1;j<=i;j++){
                num3=num1+num2;
                System.out.print(num1+" ");
                num1=num2;
                num2=num3;
            }
            
            System.out.println();
        }
    }
}
