
//reverse char num pattern
import java.io.*;

class patt7 {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter  the rows:");
        int r = Integer.parseInt(br.readLine());
        int num = r * (r + 1) / 2;
        char ch = 64;
        ch += num;
        for (int i = 1; i <= r; i++) {

            for (int j = 1; j <= i; j++) {
                if (r % 2 != 0) {
                    if (i % 2 != 0) {
                        System.out.print(ch + " ");
                    } else {
                        System.out.print(num + " ");
                    }
                } else {
                    if (i % 2 != 0) {
                        System.out.print(num + " ");
                    } else {
                        System.out.print(ch + " ");
                    }
                }
                ch--;
                num--;
                if(i%2!=0){
                    System.out.print(ch+" ");
                    }else{
                        System.out.print(num+" ");
                    }
            }

            System.out.println();
        }
    }
}
