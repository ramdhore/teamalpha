
import java.io.*;
class patt2
{
    public static void main(String[] args)throws IOException
    {
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter  the rows:");
        int r=Integer.parseInt(br.readLine());
        for(int i=1;i<=r;i++){
           
            for(int j=1;j<=r;j++){
                if(i==j){
                    System.out.print("#"+" ");

                }else{
                    System.out.print("="+" ");
                }
                
            }
            System.out.println();
        }
    }
}
