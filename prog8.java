
//4 symbol series pattern
import java.io.*;

class patt8 {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter  the rows:");
        int r = Integer.parseInt(br.readLine());
        int symb1=1;
        int symb2=2;
        int symb3=3;
        int symb4=4;

        for (int i = 1; i <= r; i++) {
            
            for (int j = 1; j <= i; j++) {
                if(i==symb1){
                    System.out.print("$"+" ");
                }
                else if(i==symb2){
                    System.out.print("@"+" ");
                }
                else if(i==symb3){
                    System.out.print("&"+" ");
                }
                else{
                    System.out.print("#"+" ");
                }
            }
            if(i==symb1){
                symb1+=4;
            }else if(i==symb2){
                symb2+=4;
            }else if(i==symb3){
                symb3+=4;
            }else{ 
                symb4+=4;
            }
            System.out.println();
        }
    }
}
